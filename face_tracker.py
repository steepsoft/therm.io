import cv2
import time
from imutils.video import VideoStream
from face_detector import FaceDetector
from multiprocessing import Manager
from servo_controller import ServoController
from temperature_sensor_controller import TemperatureSensorController


class FaceTracker:
    def __init__(self):
        # initializing the video stream
        print("Waiting for camera to warmup...")
        self.video_stream = VideoStream(0).start()
        time.sleep(2)
    
        self.haar_cascade_path = 'haarcascade_frontalface_default.xml'
        self.face_detector = FaceDetector(haar_cascade_path=self.haar_cascade_path)

        self.multiprocessing_manager = Manager()
        self.center_x = self.multiprocessing_manager.Value("i", 0)
        self.center_y = self.multiprocessing_manager.Value("i", 0)
        self.object_x = self.multiprocessing_manager.Value("i", 0)
        self.object_y = self.multiprocessing_manager.Value("i", 0)

        self.servo_controller = ServoController()
        self.temperature_sensor_controller = TemperatureSensorController()
    
    def run(self):
        while True:
            # grab the frame from the threaded video stream and flip it
            # vertically (since our camera was upside down)
            frame = self.video_stream.read()
            frame = cv2.flip(frame, 0)

            (frame_height, frame_width) = frame.shape[:2]
            self.center_x.value = frame_width // 2
            self.center_y.value = frame_height // 2

            # find the object's location
            detected_face = self.face_detector.detect(frame, (self.center_x.value, self.center_y.value))
            ((self.object_x.value, self.object_y.value), rect) = detected_face

            # extract the bounding box and draw it
            if rect is not None:
                frame = self.face_detector.draw_bounding_box(frame, rect)

                # position servos so the face is centered
                self.servo_controller.center_object(int(self.object_x.value), int(self.object_y.value))

                # assuming the frame size is 640x480
                (x, y, w, h) = rect
                if 270 < y + h < 350: 
                    if 350 < x + w < 410:
                        formatted_temperature = str(round(self.temperature_sensor_controller.calculate_object_temperature(), 2)) + " C"
                        # display temperature on screen, only when the face is centered
                        frame = self.face_detector.print_on_frame(frame, formatted_temperature, x, y - 20 if y - 20 > 0 else 0)
                
            # display the frame to the screen
            cv2.imshow("Pan-Tilt Face Tracking", frame)
            key = cv2.waitKey(1) & 0xFF
            if key == 27:
                self.cleanup()
                break
    
    def cleanup(self):
        self.servo_controller.cleanup()
        cv2.destroyAllWindows()
        self.video_stream.stop()                


if __name__ == "__main__":
    face_tracker = FaceTracker()
    face_tracker.run()