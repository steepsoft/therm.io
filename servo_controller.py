import os
import sys
import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)


class ServoController:
	def __init__(self):
		#define servo GPIOs
		self.pan_servo = int(27)
		self.tilt_servo = int(17)
		self.servo_movement_step = 3

		GPIO.setwarnings(False)
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(self.pan_servo, GPIO.OUT)
		GPIO.setup(self.tilt_servo, GPIO.OUT)

		# initial servo angle values
		self.initial_pan_angle = 90
		self.initial_tilt_angle = 70
		
		# adjusting the position of servos to the initial angle values
		self.position_servos(self.pan_servo, self.initial_pan_angle)
		self.position_servos(self.tilt_servo, self.initial_tilt_angle)

		self.pan_angle = self.initial_pan_angle
		self.tilt_angle = self.initial_tilt_angle

		

	
	def position_servos(self, servo, angle):
		servo = int(servo)
		angle = int(angle)

		# limit servo movement
		assert angle >=30 and angle <= 150
		pwm = GPIO.PWM(servo, 50)
		pwm.start(8)
		duty_cycle = angle / 18. + 3.
		pwm.ChangeDutyCycle(duty_cycle)
		time.sleep(0.1)
		pwm.stop()

	# position servos so the detected face is centered in frame
	def center_object(self, face_rectangle_x_point, face_rectangle_y_point):
		# assumes a 640x480 frame size
		if (face_rectangle_x_point > 335):
			self.pan_angle += self.servo_movement_step
			if self.pan_angle > 150:
				self.pan_angle = 150
			self.position_servos(self.pan_servo, self.pan_angle)
	
		if (face_rectangle_x_point < 305):
			self.pan_angle -= self.servo_movement_step
			if self.pan_angle < 40:
				self.pan_angle = 40
			self.position_servos(self.pan_servo, self.pan_angle)

		if (face_rectangle_y_point > 250):
			self.tilt_angle += self.servo_movement_step
			if self.tilt_angle > 140:
				self.tilt_angle = 140
			self.position_servos(self.tilt_servo, self.tilt_angle)
	
		if (face_rectangle_y_point < 230):
			self.tilt_angle -= self.servo_movement_step
			if self.tilt_angle < 30:
				self.tilt_angle = 30
			self.position_servos(self.tilt_servo, self.tilt_angle)
	
	def cleanup(self):
		# adjusting the position of servos to the initial angle values
		self.position_servos(self.pan_servo, self.initial_pan_angle)
		self.position_servos(self.tilt_servo, self.initial_tilt_angle)
		GPIO.cleanup()
