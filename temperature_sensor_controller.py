import time
import board
import busio as io
import adafruit_mlx90614


class TemperatureSensorController:
   def __init__(self):
      self.i2c = io.I2C(board.SCL, board.SDA, frequency=100000)
      self.mlx = adafruit_mlx90614.MLX90614(self.i2c)
   
   def ambient_temperature(self):
      return self.mlx.ambient_temperature
   
   def object_temperature(self):
      return self.mlx.object_temperature
   
   def print_ambient_temperature(self):
      print("Ambient Temp: ", self.mlx.ambient_temperature)

   def print_object_temperature(self):
      print("Object Temp: ", temperature_sensor_controller.object_temperature)
   
   def celsius_to_kelvin(self, temperature):
    return temperature + 273.15

   def calculate_object_temperature(self):
      object_temperature_k = self.celsius_to_kelvin(self.mlx.object_temperature)
      ambiant_temperature_k = self.celsius_to_kelvin(self.mlx.ambient_temperature)
      object_temperature_adapted_k = pow((((pow(object_temperature_k,4)-pow(ambiant_temperature_k,4))/0.89) + pow(ambiant_temperature_k,4)), 0.25)
      return object_temperature_adapted_k - 273.15


if __name__ == "__main__":
   temperature_sensor_controller = TemperatureSensorController()
   
   while True:
      # temperature results in Celsius
      temperature_sensor_controller.print_ambient_temperature()
      temperature_sensor_controller.print_object_temperature()      
      time.sleep(0.5)