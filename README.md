## THERM.IO software setup

### Hardware
1. [Raspberry 3B +](https://cleste.ro/raspberry-pi-3-model-b-plus.html)
2. [Raspberry V2 Camera](https://cleste.ro/camera-raspberry-pi-v2.html)
3. [Temperature IR sensor](https://www.optimusdigital.ro/ro/senzori-senzori-de-temperatura/3611-senzor-infrarou-de-temperatura-fara-contact-mlx90614esf-dci.html?search_query=mlx90614&results=5)
4. [Pan-Tilt Mechanism](https://cleste.ro/suport-camera-cu-servomotor-pan-tilt.html)
5. [SG90 Servo x 2](https://cleste.ro/motor-servo-sg90-9g-180grade.html)
6. [Male-Male Cables](https://cleste.ro/10-x-fire-dupont-tata-tata-10cm.html) | [Female-Male Cables](https://cleste.ro/10xfire-dupont-mama-tata-30cm.html)
7. 3D printed case


### Software
## Environment
1. Raspbian Buster OS
2. Virtual Python 3 environment

# Needed Python packages
1. OpenCV + contrib
2. adafruit_mlx90614 library
3. RPi.GPIO
4. busio
5. imutils

### Running the scripts
`python face_tracker.py`
